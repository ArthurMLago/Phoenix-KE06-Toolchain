// Nunca jamais mecher nisso em hipotese alguma:
unsigned int flashProtectionField[] __attribute__ ((section(".cfmconfig"))) = {
	0x01020304,
	0x05060708,
	0xffffffff,
    0xfebeffff
};

// Explicação:
/*
 * Para projetos profissionais, as vezes é interessante depois que o projeto está
 * terminado e pronto para vender, bloquear o micro para programação futura. Isso é
 * feito para que terceiros não possam inserir código malicioso no hardware, ou
 * ter acesso a informações sigilosas, então é realizado essa trava para que
 * não se possa mais entrar em modo de debug no micro e/ou bloquear a memória flash
 * do micro(a que armazena o programa e talvez alguns dados não volateis) contra
 * edições futuras.
 *
 * No nosso caso, raramente iremos precisar fazer isso, já que ninguém mais tem acesso
 * aos códigos da equipe além de membros, então é vantajoso deixarmos tudo destravado
 * para debugar com facilidade.
 *
 * No entanto, por algum motivo, esse novo micro estava sendo travado toda vez que um 
 * novo programa era enviado, nos forçando a limpar toda a memória flash antes de
 * poder enviar um novo programa. Como isso ia ficar muito chato depois de um tempo,
 * eu(Arthur) e JM começamos a procurar como modificar essas autorizações, e descobrimos
 * que tais configurações eram feitas em 4 bits de um registrador de controle da memória flash,
 * e que para edita-los no momento da programação, era necessário usar um dos endereços
 * do linker script(o arquivo .ld na pasta Auxiliary, não entendemos muito bem como
 * ele funciona ou é feito, se alguém puder complementar isso, sinta-se a vontade).
 * 
 * JM encontrou 1 resposta(e apenas 1) que explicava como fazer isso, e o resultado
 * é o trecho de código acima, do qual editamos alguns bytes para ficar como queriamos.
 * O problema é, que se alguns dos bits forem alterados para uma combinação especifica,
 * o micro será brickado, ou seja, não vamos poder programar ele nunca mais(provavelmente é isso,
 * talvez tenha um jeito que não conhecemos), então não arrisquem, e não mexam nesse
 * trecho de código!
 *  
 * Complementando com o que entendi(JM): 
 * A descrição da memória e de suas seções especificados no arquivo linker script é escrito primeiro
 * na memória flash e é responsável pelos valores iniciais dos registradores. Na área de memória .cfmprotrom 
 * descrita pelo endereço de memória iniciado em 0x400 e tamanho 0x10 (16 em decimal) bytes teve uma seção criada
 * chamada .cfmprotect que mantém a definição de valores descrito pela seção denominada .cfmconfig acima.
 * Os valores mostrados no código acima se referem aos 16 bytes a serem incluídos a partir da posição de memória
 * 0x400. Em relação ao processo de proteção descrito acima somente o byte encontrado em 0x40D interessa (o byte be da 4a linha)
 * pois assim configura-se o micro para estado desprotegido (esses endereços estão no manual de referência, procure por FSEC).
 *
 * Como foi dito pelo Arthur e mostrado nos links abaixo existe uma combinação na qual bricka o micro 
 * (essa combinação é colocar 10 nos bits 5-4) pois assim se desabilita o mass erase e se o micro estiver protegido acontece o brick.
 * Entretanto, segundo o manual de referência, o nosso micro é diferente pois o registrador de segurança de flash (seção 18.4.2)
 * mantém os bits 5-4 reservados e sem direito ao uso. Mas para garantir, não mexam.
 *
 * Fonte:
 * Seçao cfmconfig: https://community.nxp.com/thread/113208
 * How (not) to secure my microcontroler: https://mcuoneclipse.com/2012/11/04/how-not-to-secure-my-microcontroller/
 * Unsecuring flash memory: https://community.nxp.com/thread/432140
 * Manual de referencia: http://www.nxp.com/assets/documents/data/en/reference-manuals/MKE06P80M48SF0RM.pdf
 */