#include "derivative.h"

#include "clkConfig.h"

void clockSetup(enum clkSpeed selected){
	// Mostrar bus clock no pino PTH2:
	SIM_SOPT0 |= (1 << SIM_SOPT0_CLKOE_SHIFT);

	// Selecionar divisor de tensão do gerador de Clock baseado no argumento:
	if (selected == CORE_24MHZ_BUS_12MHZ){
		ICS_C2 = ICS_C2_BDIV(1);
	}else if(selected == CORE_48MHZ_BUS_24MHZ){
		ICS_C2 = ICS_C2_BDIV(0);
	}

	// DIV1 = 1; DIV2 = 2; Bus clock sera metade do core clock, que sera igual a
	// saida de ICS
	SIM_CLKDIV = SIM_CLKDIV_OUTDIV1(0) | SIM_CLKDIV_OUTDIV2_MASK;

	// Calibrar o clock de referecnia interna para 12MHz(se BDIV dividir por 2):
	ICS_C3 = 0x5e;
	// Calibrar o ultimo bit do clock de referencia interna para o periodo ser um
	// um poquinho maior, e dar frequencias levemente abaixo de 12MHz, por seguranca
	// para quando formos usar 48MHz Core, nunca realmente exceder 24MHz no BUS.
	ICS_C4 |= 0x1;
}
