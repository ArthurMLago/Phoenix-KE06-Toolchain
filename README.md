# Sobre este repositório #

Este repositório oferece um _template_ para criação de programas no micro-controlador **KE-06** utilizado pela equipe Phoenix de robótica da UNICAMP sem uso de uma IDE, apenas com ferramentas da linha de comando e seu editor de texto favorito. O template pode ser clonado para ser usado em qualquer projeto desenvolvido nessa plataforma, o repositório deve permanecer sempre com o mínimo de código para oferecer funcionamento correto. Além disto, a pasta *guides* deverá conter guias com todo o conhecimento disponível sobre o microcontrolador, inclusive um extensivo guia de como compilar programas usando o repositório, envia-lo para o microcontrolador e debugar.

A pasta guides conterá toda documentação do micro, cada assunto terá um diretório próprio, que conterá um README formatado com [markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) explicando tudo que sabemos, e posívelmente códigos exemplo. A idéia é que formatando READMEs dessa forma, quando tivermos uma wiki, poderemos facilmente transferir toda a documentação para a wiki usando por exemplo, o programa [pandoc](http://pandoc.org/), que pode converter para formatação de wiki.

Dicas para desenvolvimento:

* Link para o reference manual: [Reference Manual no site da NXP](http://www.nxp.com/assets/documents/data/en/reference-manuals/MKE06P80M48SF0RM.pdf).

* Existe uma biblioteca de drivers para o microcontrolador disponível pela própria NXP, que pode ser baixada [aqui](http://www.nxp.com/webapp/sps/download/license.jsp?colCode=KEXX_DRIVERS_V1.2.1_DEVD). A biblioteca foi desenvolvida para ser usada com os kits de desenvolvimento FRDM-KEXX(02,04 e 06), onde o FRDM-KE06 usa o mesmo microcontrolador, então pode-se usar os códigos e exemplos disponíveis nessa biblioteca para auxiliar na criação de nosso próprio código.