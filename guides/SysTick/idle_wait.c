#include "derivative.h" /* include peripheral declarations */
#include "clkConfig.h"

volatile int delayCount = 0;

void delay(unsigned int time){
	// Contar quantas interrupções serão necessárias para obter o delay.
	// Para fazer isso, dividiriamos a variavel time pelo periodo das interrupções do Systick,
	// porém, como o período e uma potência de 2, e divisão por 2 é equivalente a um shift,
	// podemos fazer a operação abaixo, mais eficiente que uma divisão(o compilador provavelmente
	// teria feito isso sozinho).
	// Somamos 1 para a primeira interrupção que equivale ao resto da divisão pelo periodo, logo abaixo.
	delayCount = (time >> 24) + 1;
	// Carregamos o contador com o resto da divisão pelo periodo para que esse resto seja
	// considerado ja na primeira interrupção, e nas próximas o contador possa ser recarregado normalmente.
	SYST_CVR = time % 0xFFFFFF;
	// Habilitar o Systick:
	SYST_CSR |= SysTick_CSR_ENABLE_MASK;
	// Entrar em espera até ocorrer uma interrupção. Como podem haver varias interrupções
	// do systick que não desejamos que encerrem o delay, e até interrupções de outros módulos
	// que não estão relacionados, colocamos a instrução em um loop até que a contagem de interrupções
	// chegue a 0
	while(delayCount > 0){
		asm("WFI");
	}
	// Realizado o delay, desligar o módulo:
	SYST_CSR &= ~SysTick_CSR_ENABLE_MASK;

		
}

volatile unsigned int halfSecCount = 0;

int main(void){
	// Configurar Clock:
	clockSetup(CORE_24MHZ_BUS_12MHZ);

	// Definir o valor para o SysTick ser recarregado ao chegar em 0, no caso,
	// escolhemos o maior valor possível que gere interrupções com períodos que sejam 
	// potências de 2. Como o período será 1 clock a mais do que o valor de recarga do
	// Systick, carregamos o valor máximo de 24 bits menos 1.
	SYST_RVR |= 0xFFFFFE;
	// Habilitar o uso do clock do próprio processador ao invés do externo:
	SYST_CSR |= SysTick_CSR_CLKSOURCE_MASK;
	// Habilitar interrupções do SysTick:
	SYST_CSR |= SysTick_CSR_TICKINT_MASK;

	// Definir todos os pinos da porta A como saída:
	GPIOA_PDDR |= 0xff;
	// Desabilitar entradas da porta A:
	GPIOA_PIDR |= 0xff;

	// Alterna os pinos da porta A com intervalo dependendo da leitura do PWT:
	while(1){
		// Delay de um número com mais de 24 bits:
		delay(20000000);
		GPIOA_PTOR |= 0xFF;
	}

	return 0;
}


void SysTick_Handler(){
	delayCount--;
}


