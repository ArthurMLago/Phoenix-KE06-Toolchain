#include "derivative.h" /* include peripheral declarations */
#include "clkConfig.h"


volatile unsigned int halfSecCount = 0;

int main(void){
	// Configurar Clock:
	clockSetup(CORE_24MHZ_BUS_12MHZ);

	// Zerar o registrador:
	SYST_RVR &= ~0x00FFFFFF;
	// Definir o valor para o SysTick ser recarregado ao chegar em 0, no caso,
	// escolhemos o valor para gerar um período de interrupçnao de meio segundo.
	SYST_RVR |= 12000000 - 1;
	// Habilitar o uso do clock do próprio processador ao invés do externo:
	SYST_CSR |= SysTick_CSR_CLKSOURCE_MASK;
	// Habilitar interrupções do SysTick:
	SYST_CSR |= SysTick_CSR_TICKINT_MASK;
	// Habilitar o contado do SysTick:
	SYST_CSR |= SysTick_CSR_ENABLE_MASK;

	// Definir todos os pinos da porta A como saída:
	GPIOA_PDDR |= 0xff;
	// Desabilitar entradas da porta A:
	GPIOA_PIDR |= 0xff;

	// Alterna os pinos da porta A com intervalo dependendo da leitura do PWT:
	while(1){
		asm("nop");
	}

	return 0;
}


void SysTick_Handler(){
	halfSecCount++;
	GPIOA_PTOR |= 0xff;
}


