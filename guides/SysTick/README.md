# System Tick Timer (SysTick)

## Descrição

O System Tick Timer(abreviado para SysTick) é um módulo integrado ao núcleo Cortex-M0+ do microprocessador(assim como NVIC) cuja implementação é opcional, dependendo do modelo do microcontrolador. No caso do MKE06, ele é implementado, e podemos usa-lo para contar pulsos do clock Core para gerar interrupções periódicas, ou consultar por polling normalmente.

O SysTick é basicamente um contador de 24-bits ativado por um clock que pode ser ou do Core, ou externo, que é carregado com um valor configurável no registrador **SYST_RVR**, e é decrementado a cada pulso de clock, ao atingir 0, pode gerar uma interrupção, ou apenas habilitar a flag **COUNTFLAG** no registrador **SYST_CSR**. O valor atual do contador pode ser consultado no registrador **SYST_CVR**.

A documentação para o SysTick encontra-se no siste da ARM, [neste link](http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dai0179b/ar01s02s08.html), enquanto que a descrição de cada registrador pode ser encontrada [neste link](http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dui0552a/Babieigh.html)

## Configurações

* **SYST_CSR**[**CLKSOURCE**] : 0 para usar um clock de referência externa, ou 1 para usar o próprio clock Core do processador.

* **SYST_CSR**[**TICKINT**] : 1 habilita interrupções do SysTick quando o contador atingir 0. Note que como o SysTick é um modulo integrado ao núcleo do microcontrolador, essa interrupção não é controlado pelo NVIC como os outros módulos, que são separados do Core(chamados de periféricos). Isso significa que habilitar esta flag já é o suficiente para começar a receber interrupções, sem ser necessário realizar mais configurações.
*Nota: aparentemente, interrupções geradas pelo próprio núcleo são chamadas de exceções(exceptions). Na verdade, eu posso estar mais errado, e na verdade, interrupções são exceções, mas exceções geradas pelo Core não são interrupções, como da a entender [este documento](http://www.ic.unicamp.br/~celio/mc404-2013/arm-manuals/ARM_exception_slides.pdf)*

* **SYST_CSR**[**ENABLE**] : 1 para habilitar o contador

* **SYST_RVR** : Os 24 bits menos significativos são o valor com que o contador será resetado após atingir 0.
**Importante**: Se deseja-se gerar um evento a cada **n** clocks, deve-se carregar **n - 1** nesse registrador. Faça o exemplo quando deseja-se contar 2 clocks para se convencer. O contador é carregado com 1, no próximo pulso, vai para 0 e gera um evento, no próximo pulso é carregado com 1 novamente, e no próximo vai para 0 e gera outro evento. Note que assim obtemos 1 evento a cada 2 clocks.

## Exemplos de uso

### Cronômetro
Pode-se usar o SysTick para gerar interupções periódicas, no arquivo *cronometer.c*, geramos uma interrupção a cada aproximadamente meio segundo, e contamos o número de interrupções em uma variável, assim criamos um tipo de cronômetro. Para uma leitura mais precisa, pode-se usar o valor atual do contador em **SYST_CVR** para ver quanto faltaria para próxima interrupção.

*O exemplo está no arquivo cronometer.c*


### Delay
Pode-se usar o SysTick para gerar delays sem necessariamente deixar o processador trabalhando(busy-waiting), colocando o processador em espera por uma interrupção usando a instrução **WFI**.

*O exemplo está no arquivo idle_wait.c*


## Notas 
* Podemos também usar o módulo periférico PIT(Periodic Interrupt Timer) para gerar interrupções periódicas.

