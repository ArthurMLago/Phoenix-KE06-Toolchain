# Módulo PWT 

## Descrição 

O módulo PWT é um hardware dedicado a medir a largura de pulsos de um dos dois pinos externos(**PWT_IN0**( nos pinos **PTD5**  PTE2**) e **PWT_IN1**( nos pinos **PTB0** ou **PTH7**)) do micro-controlador ou de umas das duas conexões internas do microcontrolador(**PWT_IN2** conectado ao sinal **ACMP0_OUT** e **PWT_IN3** conectado aos módulos **UART**).

Uma inconvenência do módulo é que existe apenas 1 contador e circuito de captura, significando que pode-se medir os pulsos de apenas 1 das 4 entradas em um dado momento, se deseja-se medir mais de 1 das entradas, deve-se alternar entre as entradas que deseja-se amostrar usando o valor **PINSEL** presente no registrador **PWT_R1**.

Vale lembrar também que se for usar entrada de um pino externo(**PWT_IN0** ou **PWT_IN1**) deve-se conferir que o estas entradas estão mapeadas para os pinos desejados no registrador **SIM_PINSEL1**.

Para correta utilização desse módulo, recomenda-se além de ler este README, leia a seção Functional Description do módulo PWT na página 501 do [Reference Manual](http://www.nxp.com/assets/documents/data/en/reference-manuals/MKE06P80M48SF0RM.pdf).

## Configurações 

### PWT_R1 

* **PCLKS** : Possiblita escolher entre o clock comum para o FTM e PWT(ver na figura que descreve a divisão de clocks na página 112 do [Reference Manual](http://www.nxp.com/assets/documents/data/en/reference-manuals/MKE06P80M48SF0RM.pdf)) ou um clock externo seeciona em **SIM_PINSEL0**

* **PINSEL** : Seleciona qual PWTIN[0..3] usar.

* **EDGE** : Seleciona em qual borda(subida ou descida) iniciar a medição do pulso, e em qual borda gerar uma captura. Para mais detalhes, leia a descrição do sinal na descrição do registrador **PWT_R1** e a seção de Functional Description do módulo PWT. *Aparentemente os modos que tem captura em apenas 1 borda medem apenas o período, e os modos que tem captura em borda de descida e subida capturam o tempo em alto e em baixo separadamente*.
Note que para mudanças nessa configuração tomarem efeito, é necessário realizar um Soft Reset com **PWTSR**, ou desligar e ligar novamente o módulo com **PWTEN**.

* **PRE** : Seleciona divisor de clock.

* **PWTEN** : Habilita o funcionamento do módulo. Habilite o módulo apenas após realizar todas as configurações desejadas.

* **PWTIE** : Habilita interrupções do módulo. Note como ainda será necessário habilitar as interrupções do módulo PWT no NVIC.

* **PRDYIE** : Habilita interrupção quando houver novos dados prontos para serem lidos, ou seja, quando **PWTRDY** estiver setado.

* **POVIE** : Habilita interrupção quando houver overflow do contador, ou seja, quando **PWTOV** estiver setado

* **PWTSR** : Escreva 1 para realizar um SOft reset no módulo, descartando medições atuais, e consolidando configurações novas. Sempre lê 0

* **PWTRDY** : Indica que os registradores com valores de medição foram atualizados e contém nova informação para ser lida. Após ler os novos dados, deve-se escrever 0 nesse bit para que ele não gere mais interrupções, e seja setado novamente na próxima medição.

* **PWTOV** : Indica que houve overflow do contador(0xFFFF para 0x0000). Escreva 0 para cessar interrupções, e para que possa ser setado novamente caso ocorra outro Overflow.

## Exemplos de uso

### Leitura de PWM

Para leitura de um PWM, a informação relevanta é a proporção de tempo que o pulso esteve alto em relação ao tempo baixo. É necessário conhecer a frequência do PWM para calcular o pre-scaler do clock.

Como um exemplo, suponha um pulso de PWM com frequência 200Hz, ou seja período de 5mS. Temos um contador de 16 bits, ou seja, pode contar no máximo 65536 pulsos de clock antes de gerar um overflow. Se o modulo estiver sendo alimentado com um clock de 48MHz, 65536 será equivalente a 1.3mS. Não será possível contar um período inteiro de PWM sem gerar um overflow do contador. Se for necessário ter precisão de 20nS (periodo do Clock), pode-se usar as interrupções de overflow para contar o número de overflows até que uma captura seja gerada. Na maioria dos casos, isso é desnecessário, e podemos usar o prescaler do módulo para dividir o clock até que o tempo necessário para um overflow seja menor que o periodo do PWM. Pelas contas, um pre-scaler de 4 seria o suficiente, demoraria 5.2mS para gerar um overflow no contador, tempo maior que o periodo do PWM, então poderiamos facilmente realizar a leitura dos tempos em Alto(lido nos 16 bits mais significativos de **PWT_R1**) e em baixo(lido nos 16 bits menos significativos de **PWT_R2**) e ainda mantendo precisão de 80nS(4 vezes o periodo do clock). É perigoo usar a interrupção de overflow para ativar o failsafe, pois nos caso em que o PWM é 100% ou 0%, o módulo não irá detectar nenhuma borda, e gerará diversos overflow, porém isso não significa que necessariamente não existe sinal. Neste caso, podemos usar a interrupção de overflow para definir alguma ação especial para os casos de PWM em 100%, já que a largura dos pulsos não poderá ser medida.

*Exemplo disponível no arquivo read_pwm.c*

### Leitura de receptores de rádio comprados

Radios comprados acompanham um receptor que tem como saída pulsos de PWM onde a informação relevante é contida apenas no tempo em alto do pulso, que costuma variar entre 0.5mS e 1.5mS, ou valores próximos. O período do PWM em geral é irrelevante, e estados de 100% em alto e 100% em baixo não ocorrem. Pode-se usar o módulo PWT para ler um canal destes receptores, porém, será necessário saber aproximadamente o período do PWM para evitar overflows do contador. O período do PWM costuma ser dependente do número de canais do receptor, e para o exemplo dado nesse guia(onde foi usado um arduino com a classe Servo), foi necessário configurar o módulo para gerar overflow após aproximadamente 22mS sem pulsos, apesar de que o período exato do PWM é desconhecido.
A estratégia para evitar overflows é a mesma adotada no caso da leitura do PWM, porém, se deseja-se manter maior precisão na leitura do pulso, pode-se fazer as contas para não haver overflow apenas no tempo do pulso em alto, ou seja, aproximadamente 2ms, e fazer com que o failsafe seja ativado apenas após uma determinada quantidade de overflows, ou seja, muito tempo sem bordas, e portanto não há entrada.

*Exemplo disponível no arquivo read_radio.c*

## Notas

* Assim que o PWT é ativado, o contador começa a contar pulsos de clock, possivelmente gerando overflow caso não seja detectado uma borda antes de 2^16 pulsos de clock. Assim que uma borda como a configurada for detectada, o contador é zerado, e continua contando até uma próxima borda configurada, quando será zerado novamente, e os valores dos registradores de leitura são atualizados.

* Nos códigos exemplo, habilitamos o resistor de pull-up embutido no pino de entrada. Fazemos isso pois, se soltarmos o fio de, por exemplo, um receptor, e o deixarmos solto, a tensão nesse fio pode oscilar de formas misteriosas, que podem causar bordas para o PWT. habilitando o resistor de pull-up interno, quando não existe outro elemento induzindo tensão no pino, o resistor conectado ao VCC fará com que a tensão do pino permaneça em alto, sem bordas.