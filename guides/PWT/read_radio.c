/*
 * main implementation: use this 'C' sample to create your own application
 *
 */
#include "derivative.h" /* include peripheral declarations */
#include "clkConfig.h"

volatile int delayQnt = 50000;
volatile enum{
	NORMAL,
	FAILSAFE
} state = NORMAL;

void delay(unsigned int time){
	while(time > 0)
		time -= 1;
}

int main(void){
	// Configurar Clock:
	clockSetup(CORE_24MHZ_BUS_12MHZ);

	// Ligar clock para PWT:
	SIM_SCGC |= SIM_SCGC_PWT_MASK;
	// Selecionar pino PTD5 para funcionar como entrada do PWT:
	SIM_PINSEL1 &= ~SIM_PINSEL1_PWTIN0PS_MASK;

	// Selecionar inicio de medições como uma borda de subida no pino de entrada
	PWT_R1 |= PWT_R1_EDGE(0b01);
	// Configurar o prescaler para dividir o clock de entrada por 8:
	PWT_R1 |= PWT_R1_PRE(0b011);
	// Habilita interrupção do módulo PWT:
	PWT_R1 |= PWT_R1_PWTIE_MASK;
	// Habilita interrupção quando houver uma leitura pronta:
	PWT_R1 |= PWT_R1_PRDYIE_MASK;
	// Habilita interrupção quando houver overflow:
	PWT_R1 |= PWT_R1_POVIE_MASK;
	// Ativa modulo PWT:
	PWT_R1 |= PWT_R1_PWTEN_MASK;

	// Ligar resistor de pull-up no pino PTD5:
	PORT_PUE0 |= 1 << 29;

	// Habilitar interrupções PWT para o NVIC:
	NVIC_ISER |= 1 << 29;
	// Definir prioridade da interrupção PWT:
	NVIC_IPR7 |= 0b11 << 14;

	// Definir todos os pinos da porta A como saída:
	GPIOA_PDDR |= 0xff;
	// Desabilitar entradas da porta A:
	GPIOA_PIDR |= 0xff;


	// Alterna metade dos pinos do micro dependendo do estado do PWT:
	while(1){
		if (state == NORMAL){
			GPIOA_PTOR |= 0xff;
			delay(25 * delayQnt);
		}else{
			GPIOA_PCOR |= 0xff;
			delay(500000);
		}
	}
	
	return 0;
}


void PWT_IRQHandler(){
	// Se for uma interrupção de overflow:
	if (PWT_R1 & PWT_R1_PWTOV_MASK){
		// Limpar bit de overflow:
		PWT_R1 &= ~PWT_R1_PWTOV_MASK;
		// Entrar em failsafe:
		state = FAILSAFE;
		// Terminar tratamento de interrupção:
		return;
	}
	// Ler tempo em alto:
	delayQnt = (PWT_R1 & PWT_R1_PPW_MASK) >> PWT_R1_PPW_SHIFT;
	// Limpar flag de dado pronto:
	PWT_R1 &= ~PWT_R1_PWTRDY_MASK;
	// Se estava em failsafe, não vai estar mais:
	state = NORMAL;
	}
}