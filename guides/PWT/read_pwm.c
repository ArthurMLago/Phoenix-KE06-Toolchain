/*
 * main implementation: use this 'C' sample to create your own application
 *
 */
#include "derivative.h" /* include peripheral declarations */
#include "clkConfig.h"

volatile int upTime = 1, downTime = 1;

void delay(unsigned int time){
	while(time > 0)
		time -= 1;
}

int main(void){
	// Configurar Clock:
	clockSetup(CORE_24MHZ_BUS_12MHZ);

	// Ligar clock para PWT:
	SIM_SCGC |= SIM_SCGC_PWT_MASK;
	// Selecionar pino PTD5 para funcionar como entrada do PWT:
	SIM_PINSEL1 &= ~SIM_PINSEL1_PWTIN0PS_MASK;

	// Selecionar inicio de medições como uma borda de subida no pino de entrada
	PWT_R1 |= PWT_R1_EDGE(0b01);
	// Habilita interrupção do módulo PWT:
	PWT_R1 |= PWT_R1_PWTIE_MASK;
	// Habilita interrupção quando houver uma leitura pronta:
	PWT_R1 |= PWT_R1_PRDYIE_MASK;
	// Habilita interrupção quando houver overflow:
	PWT_R1 |= PWT_R1_POVIE_MASK;
	// Ativa modulo PWT:
	PWT_R1 |= PWT_R1_PWTEN_MASK;

	// Ligar resistor de pull-up no pino PTD5:
	PORT_PUE0 |= 1 << 29;

	// Habilitar interrupções PWT para o NVIC:
	NVIC_ISER |= 1 << 29;
	// Definir prioridade da interrupção PWT:
	NVIC_IPR7 |= 0b11 << 14;

	// Definir todos os pinos da porta A como saída:
	GPIOA_PDDR |= 0xff;
	// Desabilitar entradas da porta A:
	GPIOA_PIDR |= 0xff;


	// Alterna metade dos pinos do microcontrolador dependendo da leitura do PWT:
	while(1){
		GPIOA_PTOR |= 0xff;
		delay(5 * upTime);
		GPIOA_PTOR |= 0xff;
		delay(5 * downTime);
	}
	return 0;
}

void PWT_IRQHandler(){
	// Verifica se é uma interrupção de overflow, se não for, é de dado novo:
	if (PWT_R1 & PWT_R1_PWTOV_MASK){
		// Limpar bit de overflow:
		PWT_R1 &= ~PWT_R1_PWTOV_MASK;
		// Usando GPIO: ver qual o estado atual do pino:
		if (GPIOA_PDIR & (1 << 29)){
			// PWM está em 100%, então:
			upTime = 5000;
			downTime = 0;
		}else{
			// PWM está desligado, então:
			upTime = 0;
			downTime = 5000;
		}
		// Encerrar tratamento de interrupção:
		return;
	}
	// Limpar bit de informação pronta:
	PWT_R1 &= ~PWT_R1_PWTRDY_MASK;
	// Obter tempo alto do pwm:
	upTime = (PWT_R1 & PWT_R1_PPW_MASK) >> PWT_R1_PPW_SHIFT;
	// Obter tempo baixo do pwm:
	downTime = PWT_R2 & PWT_R2_NPW_MASK;
}