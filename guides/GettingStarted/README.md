
# Instruções para programar o micro-controlador KE06 #

## Preparação do computador ##

### Explicação do diretório ###

### Instalação do OpenOCD ###

### Instalação do GCC para ARM ###
### Arquivos de Configuração do OpenOCD ###
_**TODO:explicar como OpenOCD funciona**_

No diretório OpenOCD config, existem arquivos de formato _.cfg_ criados para facilitar o uso do OpenOCD para ser necessário usar apenas 1 destes arquivos para inicializar o OpenOCD.

### Ajustes necessários para uso do Makefile ###
Após clonar o diretório, haverá um Makefile na raiz preparado para compilar programas para o microcontrolador. Antes de usa-lo, deve-se baixar a biblioteca EWL(Embedded Warrior Libraries), ela pode ser obtida de uma instalação do CodeWarrior(no nosso caso, conseguimos a pasta copiando dos computadores da LE30, e deixamos disponível no Drive: [Pasta EWL](https://drive.google.com/open?id=0B9O1H9WQ1UqRWk1PTWxoRmFKU3c)).

Caso precise, pode aprender mais sobre [[makefiles]] nesse artigo(que não temos, mas deixei aqui para o dia que tivermos, mas você ainda pode aprender a respeito nesse [link](http://mrbook.org/blog/tutorials/make/)) para acompanhar os próximos parágrafos.

Será necessário colocar a pasta descompactada em algum diretório do seu computador, e copiar o arquivo Makefile-example para um arquivo Makefile no diretório do projeto. neste novo arquivo Makefile criado é necessário alterar a variável _EWL\_Path_ de modo quecontenha o caminho para a pasta.

As variáveis **SOURCES** e **AUX** devem conter os nomes dos arquivos nas pastas _Sources_ e _Auxiliary_ que devem ser compilados. Então se você criou diversos arquivos .c e seus respectivos .h para usar como bibliotecas, deverá adicionar os nomes dos arquivos .c na variável **SOURCES**, mas se fizer um arquivo que tem como única função ser importado em outro arquivo e não deve ser compilado, então ele **não** deve ser adicionado a variável **SOURCE**. Deve-se evitar alterar a variável **AUX**, ela deve conter arquivos que são usados para inicialização do micro, indepente de qual o programa a ser utilizado.

O resto das varíaveis do Makefile podem ser usadas para customizar a compilação, por exemplo, pode-se estabeler quanto o programa deve estar preparado para debug com a variável **DebugLevel**, e o nível de otimização do programa através da variável **OptimizationLevel**(não tenho certeza quanta isso alteraria o programa, então por enquanto é melhor deixar em 0 para deixar sem otimização, e escrever programas bons).

## Compilando o Programa ##

Após ter-se realizado todas as configurações descritas na seção anterior, para compilar é necessário apenas navegar até o diretório do projeto pelo Terminal, e digitar __make__(assumindo que o __make__ esteja instalado no seu computador, e provavelmente estará), e o programa deverá compilar corretamente, um arquivo nomeado _**ProjectName**.elf_(onde **ProjectName** é a variável do Makefile) será colocado na pasta build, junto com diversos arquivos _.o_ que são arquivos de compilação intermediários para agilizar as próximas compilações, e um arquivo _**ProjectName**.map_ que contém informações sobre como o programa será colocado na memória do microcontrolador. O arquivo .elf é o que será usado para programar o microcontrolador com seu programa.

## Programando o microcontrolador ##

Terminado a compilação do seu programa, será o momento de realmente enviar o programa para o micro-controlador.

### Preparando o hardware ###

Temos 2 formas de programar o micro, no momento que este guia foi escrito, estamos usando apenas o antigo debugger da XMC4500 Relax Kit, uma pequena placa com um header de 8 pinos que devem ser conectados em pinos específicos do micro-controlador.

O microcontrolador KE-06 só pode ser programado usando o protocolo [[SWD]], um protocolo que de comunicação que usa apenas 2 pinos(**SWD_DIO** e **SWD_CLK**) para comunicar o microcontrolador com o debugger, além disso é necessário dar acesso ao pino **RESET** do micro para debugar, pois a programação só pode ser realizada durante o modo de inicialização do micro, então o debugger precisa forçar continuamente o modo de reset até a programação ser concluída. No total será necessário conectar então os 2 pinos de SWD, o pino de reset, o GND(VSS), e por último, o VCC, caso for usar a alimentação do debugger para alimentar o micro(totalizando 4/5 pinos). Esse pinos devem ser cuidadosamente conectados aos devidos pinos do micro como nas figuras abaixo:

![Foto do hardware para programar o micro](relaxPinout.png)

![Diagrama de pinos do micro e Debugger, e como devem estar conectados](guide/)

### Programando o micro ###

Com o micro devidamente conectado(e vefifique bem se as conexões foram feitas corretamente!!!), pode conectar o debugger via USB no computador, e preparar 3 janelas diferentes do terminal para começar a trabalhar.

Em Uma delas, você deverá navegar até o diretório do projeto e inicializar o OpenOCD, que se comunicará com o debugger, através do comando:

`openocd -f ./OpenOCD_config/phoenix-ke06-relax.cfg`

Se tudo acontecer como esperado, ele deve apenas deixar o terminal ocupado. Em outra janela do terminal, abra uma conexão com o servidor que o OpenOCD criou e está mantendo utilizando o comando

`telnet localhost 4444`

Esse comando abrirá um console interativo no terminal, onde você pode usar diversos comandos para se comunicar com o OpenOCD, e por consequência, o debugger. O comando mais importante no momento será

`program build/ProjectName.elf`

Note que o caminho passado como argumento, é relativo ao diretório onde o OpenOCD foi inizializado na janela anterior. Este comando toma um arquivo _.elf_ e usa-o para programar o micro de acordo. Após a programação ser concluída com sucesso, você pode usar o comando 

`reset`

para iniciar o programa.

Além destas 2 janelas para operar o OpenOCD, recomendo o uso de uma terceira janela no diretório do projeto para compilar programas novos.


## Debugando o programa ##

### Iniciando o GDB ###

Em mais uma janela do terminal(totalizando 4 janelas), você pode abrir o **GDB** para debugar o programa com _breakpoints_, verificar e editar endereços de memória, verificar conteúdo de registradores, etc. Para iniciar o gdb, navegue até o diretório do projeto e use o comando:

`arm-none-eabi-gdb build/ProjectName.elf`

Note que essse comando toma como argumento o arquivo _.elf_ programado no micro, caso você reprograme o micro, mesmo que com o mesmo nome de arquivo, será necessário atualizar o arquivo do gdb usando:

`file build/ProjectName.elf`

Feito isso, use o seguinte comando para conectar o gdb ao servidor do OpenOCD:

`target remote localhost:3333`

Conectado ao servidor, você pode sincronizar o gdb com o microcontrolador usando o comando:

`monitor reset halt`

Existe uma regra no makefile que pode ser invocada usando _make gdb_, que executará todos os passos de inicialização do GDB descritos acima, com exceção do comando que atualiza o arquivo, desnecessário já que a inicialização do gdb já toma como parâmetro o programa a ser debugado.

O comando _monitor reset halt_ irá também parar a execução do programa, para iniciar a execução, use o comando.

`continue`

ou a versão mais curta:

`c`

### Pausando a execução ###

Após teclar `continue`, o programa irá rodar até que seja interrompido, até lá, o gdb não responderá a seus comandos. Para interromper a execução do programa e voltar a poder realizar comandos, você pode apertar **ctrl + C**, e o GDB irá interromper a execução imediatamente.

Além disto você pode definir até **2**(limitação do micro-controlador KE-06) breakpoints, então o GDB irá interromper a execução imediatamente antes de executar a linha onde o breakpoint foi posicionado.

Para posicionar um breakpoint no código, usa-se `break` ou `b`  

### Imprimindo e Alterando Endereços de Memória ###

Uma das tarefas mais básicas para debugar programas em micro-controladores é a de imprimir/alterar o conteúdo de endereços de memória, pois é assim que mexemos em registradores vitais do sistema sem ter que reiniciar a execução do programa.

Para imprimir o conteúdo de um endereço de memória, segue-se as instruções [desta página](http://www.delorie.com/gnu/docs/gdb/gdb_56.html). Alguns exemplos para imprimir memórias:

**Imprimir um registrador de 32 bits no endereço _0x4004\_8030_:**
`x/1xw 0x40048030`

**Imprimir 4 registradores 8 bits começando no endereço _0x2000_2340_:**
`x/4xb 0x20002340`

**Alterar o conteúdo de um endereço de memória**

 Usa-se(aparentemente isso é um tipo de migué, mas funciona muito bem, e é bem simples), por exemplo, para setar um endereço de memória de 32bits:

`set {int}0x08040000 = 0xfffdddff`

Para setar um endereço de apenas 8 bits:

`set {unsigned char}0x08041120 = 0x5d`


### Imprimindo e alterando registradores de propósito geral ###

Esta seção provém [desta página](https://sourceware.org/gdb/onlinedocs/gdb/Registers.html).

Para verificar o conteúdo dos registradores do processador(e com isso, quero dizer o CORE mesmo, que executa instruções, e tem que carregar dados nos seus registradores internos para realizar operações), usa-se o comando: 

`info registers`

É possível alterar o conteúdo dos registradores, por exemplo, aumentar o endereço da pilha em 4:

`set $sp += 4`



