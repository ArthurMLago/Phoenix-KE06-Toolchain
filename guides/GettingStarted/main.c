#include "derivative.h" /* include peripheral declarations */
#include "clkConfig.h"

void delay(unsigned int time){
	while(time > 0)
		time -= 1;
}

int main(void){
	// Configurar Clock:
	clockSetup(CORE_24MHZ_BUS_12MHZ);

	// Definir todos os pinos da porta A como saída:
	GPIOA_PDDR |= 0xff;
	// Desabilitar entradas da porta A:
	GPIOA_PIDR |= 0xff;

	// Alterna os pinos da porta A com intervalo dependendo da leitura do PWT:
	while(1){
		GPIOA_PTOR |= 0xff;
		delay(500000);
	}
	
	return 0;
}