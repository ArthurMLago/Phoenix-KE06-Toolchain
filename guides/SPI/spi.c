#include "derivative.h" /* include peripheral declarations */
#include "clkConfig.h"

// #if CLOCK_SEL == CORE_24MHZ_BUS_12MHZ
//     #define uSEC_TO_DELAY_CLK(x) 

void sendNextIfAvailable();

void delay(unsigned int time){
	while(time > 0)
		time -= 1;
}

int contagem = 0;

volatile unsigned int matrixRecepcao[20][8];

volatile unsigned char SPI0v_ReceivedByte = 0;
volatile int SPI0v_TamPacote = 0;
volatile char SPI0v_Busy = 0;

char vetor[32];

int main(void){
	// Configurar Clock:
	clockSetup(CORE_24MHZ_BUS_12MHZ);
	// Habilita o clock para o módulo SPI0:
	SIM_SCGC |= SIM_SCGC_SPI0_MASK;
	// Seleciona os pinos da porta PTE pra operar como SPI:
	SIM_PINSEL0 |= SIM_PINSEL_SPI0PS_MASK;


	// Configurar NVIC:

	// Ligar interrupções do SPI0:
	NVIC_ISER |= 1 << 10;
	// Definindo a prioridade da interrupção do SPI0:
	NVIC_IPR2 |= 0b11 << 22;

	// Habilita interrupcao de buffer cheio:
	// SPI0_C1 |= SPI_C1_SPIE_MASK;
	// Habilita interrupção quando termina de enviar byte:
	// SPI0_C1 |= SPI_C1_SPTIE_MASK;
	// Configura como master:
	SPI0_C1 |= SPI_C1_MSTR_MASK;
	// Configura polaridade do clock para ativo alto:
	SPI0_C1 &= ~SPI_C1_CPOL_MASK;
	// Configura a primeira borda de clock para ocorrer no meio do primeiro ciclo de transferencia:
	SPI0_C1 &= ~SPI_C1_CPHA_MASK;
	// Mandar e receber o bit mais significativo primeiro:
	SPI0_C1 &= ~SPI_C1_LSBFE_MASK;


	// SSOE nao importa quando MODFEN = 0:
	// Junto com o bit C2[MODFEN] = 1 faz o SS operar automaticamente: 
	// SPI0_C1 |= SPI_C1_SSOE_MASK;

	// Desabilitar hardware match:
	SPI0_C2 &= ~SPI_C2_SPMIE_MASK;
	// Desabilita mode fault, e tambem desabilita SS automatico
	SPI0_C2 &= ~SPI_C2_MODFEN_MASK;
	// Modulo continua operando em modo de espera do core:
	SPI0_C2 &= ~SPI_C2_SPISWAI_MASK;
	// Configura para comunicao bidirecional com MISO e MOSI
	SPI0_C2 &= ~SPI_C2_SPC0_MASK;

	// Seta o prescaler para dividir por 3:
	SPI0_BR &= ~SPI_BR_SPPR_MASK;
	SPI0_BR |= SPI_BR_SPPR(0b010);
	// Seta o divisor para dividir por 2:
	SPI0_BR &= ~SPI_BR_SPR_MASK;
	SPI0_BR |= SPI_BR_SPR(0b0000);


	// Tem que ligar depois !!!!!
	// Liga o módulo:
	SPI0_C1 |= SPI_C1_SPE_MASK;

	// Configura pino do SS como saida do GPIO:
	GPIOB_PDDR |= 1 << 3;
	GPIOB_PIDR |= 1 << 3;

	// Configura pino do CE como saida do GPIO:
	GPIOB_PDDR |= 1 << 4;
	GPIOB_PIDR |= 1 << 4;

	// Deixa o CE baixo:
	GPIOB_PCOR |= 1 << 4;



	delay(5000);

	sendNextIfAvailable();

	while(1){

	}

	return 0;
}

// essa interrupcao vem quando o buffer de transmissao esta vazio. o buffer de trnasmissao eh diferente do registrador
// que esta shiftando bits para a linha do MOSI. O efeito disso, é que quando saimos do modulo parado para comercar a trnasmissao,
// a primeira interrupcao carrega 1 byte a ser trnasferido no buffer que é imediatamente trnasferido para o registrador de shift, deixando
// o buffer vazio novamente, e causando outra interrupcao onde carregamos o proximo bit. Após carregar no buffer o ultimo byte a ser transferido, deve-se
// lembrar que nesse momento o penultimo byte esta começando a ser transferido, e o ultimo byte so vai poder comecar a ser trnaferido depois que o penultimo terminar.
// Com isso, nao podemos por exemplo desligar o SS na interrupcao posterior a que transferimos o ultimo byte, pois esse ainda estara sndo transmitido. É necessário esperar mais um recebimento.
// Teremos que usar bem as flags de habilitar interrupcao de trnasferencia e recepcao.

void SPI0_IRQHandler(){
	// Verificar se o buffer de transmissão pode receber novos dados:
	// Nota: é obrigatório ler SPTED antes de enviar um dado novo.
	if (SPI0_S & SPI_S_SPTEF_MASK){
		// Se ainda exsite conteúdo a ser enviado:
		if (SPI0v_TamPacote > 0)
			// Escreve o próximo byte a ser enviado:
			SPI0_D = vetor[SPI0v_TamPacote - 1];
	}

	// Verificar se o buffer de recepção tem conteúdo novo:
	if (SPI0_S & SPI_S_SPRF_MASK){
		// Ler o byte recebido:
		if (contagem < 20){
			matrixRecepcao[contagem - 1][SPI0v_TamPacote] = SPI0_D;
		}else{
			matrixRecepcao[19][SPI0v_TamPacote] = SPI0_D;
		}
	}

	// Contar um pacote a menos a enviar
	// Nota: será 0 para indicar que não há mais nada a ser enviado, mas deve-se receber mais 1 byte
	//       será -1 para indicar que já se recebeu o ultimo byte
	SPI0v_TamPacote--;


	// Verificar se foi o ultimo byte a carregar no buffer:
	if (SPI0v_TamPacote == 0){
		// Desligar interrupção de buffer de transmissão vazio:
		SPI0_C1 &= ~SPI_C1_SPTIE_MASK;
	}

	// Verificar se o ultimo byte foi recebido:
	if (SPI0v_TamPacote == -1){
		// Subir a linha SS:
		GPIOB_PSOR |= 1 << 3;
		// Desligar interrupção de dado recebido, oque também significa que o módulo está parado:
		// Nota: é importante fazer isso depois de subir SS, do contrario,
		//       caso a execução seja interrompida neste exato ponto, a nova interrupção
		//       pode iniciar novas transferências imediatamente, antes do SS subir,
		//       assim o novo pacote pode ser iniciado sem uma pausa apropriada do SS em alto.
		SPI0_C1 &= ~SPI_C1_SPIE_MASK;

		// Se houverem mais pacotes a serem enviados, enviar:
		sendNextIfAvailable();



	}
}

void sendNextIfAvailable(){
	// Se o módulo SPI já estiver trabalhando, não fazer nada:
	if (SPI0_C1 & SPI_C1_SPIE_MASK)
		return;

	// Aqui nós deveriamos verificar se existe nova informação para ser enviada, mas no caso vamos
	if (contagem == 0){
		// Enviar 2 bytes(mas o segundo mandamos só para poder ler do micro):
		SPI0v_TamPacote = 2;
		// O byte a ser enviado primeiro, representando a leitura do registrador em 0x06.
		vetor[1] = 0x06;
//		vetor[0] = nao importa;
	}else if(contagem == 1){
		// Enviar 2 bytes:
		SPI0v_TamPacote = 2;
		// O primeiro byte a ser enviado, representa escrever no registrador em 0x06:
		vetor[1] = 0x26;
		// O conteúdo a escrever no registrador. representa <por enquanto nada>
		vetor[0] = 88;
	}else{
		// Enviar 2 bytes(mas o segundo mandamos só para poder ler do micro):
		SPI0v_TamPacote = 2;
		// O byte a ser enviado primeiro, representando a leitura do registrador em 0x06.
		vetor[1] = 0x06;
//		vetor[0] = nao importa;  
	}
	contagem++;

	// Ligar interrupção de buffer de recepção e transmissão vazios:
	SPI0_C1 |= SPI_C1_SPIE_MASK;
	SPI0_C1 |= SPI_C1_SPTIE_MASK;

	// Abaixar SS:
	// Nota: pelo mesmo motivo que devemos subir SS antes de desligar as interrupções,
	//       devemos ligar as interrupções antes de descer a linha, pois usaremos a
	//       flag de interrupção de recepção para determinar se o módulo está
	//       ocupado, e se houverem interrupções durante essa função, esta nova
	//       interrupção pode tentar usar o módulo também, causando incoerências.
	GPIOB_PCOR |= 1 << 3;




}
