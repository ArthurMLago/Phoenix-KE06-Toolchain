/* APCTL1 Bit Fields - Enable I/O Pin Control */
#define ADC_APCTL1_ADPC_AD0 				0xFFFEu
#define ADC_APCTL1_ADPC_AD1 				0xFFFDu
#define ADC_APCTL1_ADPC_AD2 				0xFFFBu
#define ADC_APCTL1_ADPC_AD3 				0xFFF7u
#define ADC_APCTL1_ADPC_AD4 				0xFFEFu
#define ADC_APCTL1_ADPC_AD5 				0xFFDFu
#define ADC_APCTL1_ADPC_AD6 				0xFFBFu
#define ADC_APCTL1_ADPC_AD7 				0xFF7Fu
#define ADC_APCTL1_ADPC_AD8 				0xFEFFu
#define ADC_APCTL1_ADPC_AD9 				0xFDFFu
#define ADC_APCTL1_ADPC_AD10 				0xFBFFu
#define ADC_APCTL1_ADPC_AD11 				0xF7FFu
#define ADC_APCTL1_ADPC_AD12 				0xEFFFu
#define ADC_APCTL1_ADPC_AD13 				0xDFFFu
#define ADC_APCTL1_ADPC_AD14 				0xBFFFu
#define ADC_APCTL1_ADPC_AD15 				0x7FFFu


/* SC1 Bit Fields */
#define ADC_SC1_ADCH_AD0					0x00u
#define ADC_SC1_ADCH_AD1					0x01u
#define ADC_SC1_ADCH_AD2					0x02u
#define ADC_SC1_ADCH_AD3					0x03u
#define ADC_SC1_ADCH_AD4					0x04u
#define ADC_SC1_ADCH_AD5					0x05u
#define ADC_SC1_ADCH_AD6					0x06u
#define ADC_SC1_ADCH_AD7					0x07u
#define ADC_SC1_ADCH_AD8					0x08u
#define ADC_SC1_ADCH_AD9					0x09u
#define ADC_SC1_ADCH_AD10					0x0Au
#define ADC_SC1_ADCH_AD11					0x0Bu
#define ADC_SC1_ADCH_AD12					0x0Cu
#define ADC_SC1_ADCH_AD13					0x0Du
#define ADC_SC1_ADCH_AD14					0x0Eu
#define ADC_SC1_ADCH_AD15					0x0Fu
#define ADC_SC1_ADCH_VSS					0x10u
#define ADC_SC1_ADCH_TEMP_SENSOR			0x16u
#define ADC_SC1_ADCH_BANDGAP				0x17u
#define ADC_SC1_ADCH_VREFH					0x1Du
#define ADC_SC1_ADCH_VREFL					0x1Eu
#define ADC_SC1_ADCH_DISABLED				0x1Fu


/* SC3 Bit Fields */
#define ADC_SC3_MODE_8BITS					0x0u
#define ADC_SC3_MODE_10BITS					0x1u
#define ADC_SC3_MODE_12BITS					0x2u

#define ADC_SC3_ADIV_DR1					0x0u
#define ADC_SC3_ADIV_DR2					0x1u
#define ADC_SC3_ADIV_DR3					0x2u
#define ADC_SC3_ADIV_DR4					0x3u

#define ADC_SC3_ADICLK_BUS					0x0u
#define ADC_SC3_ADICLK_BUS2					0x1u
#define ADC_SC3_ADICLK_ALTCLK				0x2u
#define ADC_SC3_ADICLK_ADACK				0x3u

/* SC4 Bit Fields */
#define ADC_SC4_AFDEP_DISABLED				0x0u
#define ADC_SC4_AFDEP_D2					0x1u
#define ADC_SC4_AFDEP_D3					0x2u
#define ADC_SC4_AFDEP_D4					0x3u
#define ADC_SC4_AFDEP_D5					0x4u
#define ADC_SC4_AFDEP_D6					0x5u
#define ADC_SC4_AFDEP_D7					0x6u
#define ADC_SC4_AFDEP_D8					0x7u