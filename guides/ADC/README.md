# Módulo ADC 

## Descrição 

O módulo ADC é um hardware dedicado a conversão de sinais analógicos em sinais digitais. Estas conversões podem ser realizadas em até 16 pinos de entrada analógica, que podem ser iniciadas através de trigger por software (escrevendo no registrador **ADC_SC1**) ou por hardware (sinal de entrada). Esta conversão pode ser realizada de maneira continua ou ser apenas umarequisição única. O resultado digital final da conversão pode ter precisão de 8, 10 ou 12 bits e é obtido acessando-se o registrador **ADC_R**.

O módulo possui uma opção de conversão aglomerada referenciada como modo FIFO pelo [Reference Manual](http://www.nxp.com/assets/documents/data/en/reference-manuals/MKE06P80M48SF0RM.pdf). Nela, a fim de poupar processamento e interrupções, é possível acionar até 8 canais de entradas analógicas (sendo este número previamente configurado) que são convertidas em conjunto e o resultado também é acessado pelo regitrador **ADC_R**, porém a cada acesso de leitura neste registrador o valor de conversão do próximo canal na fila é adquirido. Lembre-se que FIFO é First In First Out, ou sej, o primeiro canal de conversão configurado será o primeiro canal de conversão a ser lido.

O módulo conta ainda com a opção de comparação de dois sinais analógicos. 

Para correta utilização desse módulo, recomenda-se além de ler este README, leia a seção 24.4 - Functional Description do módulo ADC na página 342 do [Reference Manual](http://www.nxp.com/assets/documents/data/en/reference-manuals/MKE06P80M48SF0RM.pdf).

## Configurações 

### Pinos de referência

A fim deste módulo funcionar é necessário conectar os pinos de referência para a conversão. Eles são **VDDA** e **VSSA** ou então **VREFH** e **VREFL**

### ADC_APCTL1 

* **ADPC** : Conjunto de 16 bits (0-15) onde caso o bit x seja setado o controle I/O do pino ADx será desabilitado.

### ADC_SC1 

* **COCO** : Somente Leitura. Flag que indica fim de conversão (caso em modo FIFO o fim da conversão é dado somente no fim da conversão do conjunto de canais). Este bit é zerado quando **ADC_SC1** sofre operação de escrita ou quando **ADC_R** sofre operação de leitura.

* **AIEN** : Habilita interrupção de término de conversão.

* **ADCO** : Habilita conversão contínua. Em caso de requisição simples (ADCO = 0) somente uma requisção é feita, caso habilitado as conversões continuam ininterruptamente.

* **ADCH** : Seleciona qual canal analógico de entrada utilizar para conversão.

### ADC_SC2 

* **ADACT** : Somente Leitura. Flag que indica se existe uma conversão em progresso

* **ADTRG** : Seleciona modo de trigger para iniciar conversão: 0 para software trigger (escrevendo no registrador **ADC_SC1**), 1 para hardware trigger(sinal de entrada)

* **ACFE** : Habilita função de comparação de sinais analógicos.

* **ACFGT** : Configura a função de comparação para acionar quando o resultado da conversão do valor de entrada sendo monitorado: 0 para menor que valor de comparação, 1 para maior ou igual ao valor de comparação. 

* **FEMPTY** : Somente Leitura. Indica se o resultado de conversão FIFO possui nenhum novo dado válido

* **FFULL** : Somente Leitura. Indica que o resultado da conversão FIFO está cheio e que dados antigos serão sobrescritos caso nenhuma ação de leitura for tomada.

* **REFSEL** : Seleciona a tensão de referência para as conversões (00 - VREFH/VREFL (padrão), 01 - VDDA/VSSA)

### ADC_SC3 

* **ADLPC** : Habilita configuração de baixo consumo

* **ADIV** : Seleciona a razão na qual será dividido o clock utilizado pelo ADC

* **ADLSMP** : Configura tempo de amostragem longo 

* **MODE** : Seleciona a precisão da conversão (00 - 8 bits, 01 - 10 bits, 10 - 12 bits)

* **ADICLK** : Seleciona o clock de entrada.

### ADC_SC4 

* **HTRGME** : Habilita múltiplas conversões com trigger por hardware

* **ASCANE** : Habilita modo de scan para FIFO (se habilitado a conversào irá se repetir até a condição FIFO de todos canais necessários pra iniciar conversão seja garantida)

* **ACFSEL** : Seleciona função de comparação (0 - OR, 1- AND)

* **AFDEP** : Nível FIFO. Seleciona até 8 canais a serem convertidos em conjunto. Para somente 1 canal FIFO é desabilitado.

### ADC_SC5 

* **HTRGMASKE** : Habilita máscara para trigger por hardware

* **HTRGMASKSEL** : Seleciona o modo para máscara de trigger por hardware

## Acesso a Resultados

### ADC_R

Resultado das conversões de sinais analógicos


### ADC_CV

Resultado das comparações de sinais analógicos


## Exemplos de uso

*Os exemplos de uso selecionados foram escolhidos julgando o que seria mais comumulmente utilizado dentro das atividades da equipe. Portanto, trigger por hardware e comparação de sinais analógicos não foram abordados. Abaixo encontram-se o que cada um dos exemplos propôs fazer. Para uma explicação mais detalhada favor ler os comentários dos arquivos referentes a cada exemplo*

### Utilização simples

Para este exemplo foi configurado leituras frequentes em loop infinito do canal AD0 encontrado no pino **PTA0** (pino 50).

*Exemplo disponível no arquivo adc.c*

### Modo FIFO

Para testar esta funcionalidade foi configurado leituras frequentes em loop infinito dos canais AD0 encontrado no pino **PTA0** (pino 50) e AD1 encontrado no pino **PTA1** (pino 49) .

*Exemplo disponível no arquivo adc_fifo.c*