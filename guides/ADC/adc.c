#include "derivative.h" /* include peripheral declarations */
#include "clkConfig.h"
#include "ADC.h"

void delay(unsigned int time){
	while(time > 0)
		time -= 1;
}

unsigned short ADC_read(void) {

	/* ADC_SC1
 	Escrita em ADC_SC1 ativa a conversão pois foi configurado trigger por software em ADC_SC2. Em ADC_SC1 configura-se:
 		- Habilta-se interrupção quando a conversão estiver completa (AIEN)
 		- Seleciona-se o canal de entrda AD0
 		- Por omissão (bit = 0) escolhe-se uma única conversão ao inves de conversão contínua (ADCO)
 	*/
	ADC_SC1 = ADC_SC1_AIEN_MASK | ADC_SC1_ADCH(ADC_SC1_ADCH_AD0); 


	// Espera enquanto a conversão estiver em progresso (ADACT = 1)
	while(ADC_SC2 & ADC_SC2_ADACT_MASK); 

	// Aguarda o término da conversão (COCO = 1)
	while(!(ADC_SC1 & ADC_SC1_COCO_MASK)); 

	//OBS: Utilizar os 2 whiles pode ser redundante

	// Retorna o valor da conversão
	return ADC_R;
}

void ADC_init(void) {

	//Ativa clk para ADC
	SIM_SCGC |= SIM_SCGC_ADC_MASK ; 

	//Habilita o controle de I/O do pino AD0
 	ADC_APCTL1 = ADC_APCTL1_ADPC(ADC_APCTL1_ADPC_AD0);

 	/* ADC_SC3
 	Seleciona as seguintes configurações respectivamente:
 		- Baixo consumo de energia
 		- Razão 1 de divisão de clock (taxa de clock igual a clock de entrada)
 		- Tempo de amostragem longo
 		- Precisão de 8 bits
 		- Fonte do Clock de Entrada: Bus Clock
 	*/
    ADC_SC3 = ADC_SC3_ADLPC_MASK | ADC_SC3_ADIV(ADC_SC3_ADIV_DR1) | ADC_SC3_ADLSMP_MASK | ADC_SC3_MODE(ADC_SC3_MODE_8BITS) | ADC_SC3_ADICLK(ADC_SC3_ADICLK_BUS);

    /* ADC_SC2
    Nenhuma alteração é feita pois assim é configurado:
    	- Trigger de conversão feito por software através da escrita em ADC_SC1
    	- Função de Comparação é desabilitada
    	- Utiliza voltagem de referência padrão (VREFH/VREFL)
    */ 
    ADC_SC2 = 0x00;
}

int main(void) {
	// Configurar Clock:
	clockSetup(CORE_24MHZ_BUS_12MHZ);


//=====MODO SIMPLES======
	// Inicia o ADC
	ADC_init();

	unsigned short value = 1;

	while(1) {

		//Lê o valor do ADC atual
		value = ADC_read();

		delay(500000);


	}
	
	return 0;
}