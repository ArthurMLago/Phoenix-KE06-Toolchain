//CONEX�ES DO M�DULO GIRO/ACELER�METRO MPU6050 NO MICRO KE06
//SCL em PTA3 (pino 47)
//SDA em PTA2 (pino 48)
//VCC em VDD (pino 7)
//GND em VSS (pino 10)

#include <stdio.h>
#include <stdlib.h>
//#include "MKE06Z4.h"
#include "derivative.h"
#include "clkConfig.h"

#define I2C_DisableAck()       I2C0_C1 |= I2C_C1_TXAK_MASK
#define I2C_EnableAck()        I2C0_C1 &= ~I2C_C1_TXAK_MASK
#define I2C_RepeatedStart()    I2C0_C1 |= I2C_C1_RSTA_MASK
#define I2C_EnterRxMode()      I2C0_C1 &= ~I2C_C1_TX_MASK
#define I2C_EnterTxMode()      I2C0_C1 |= I2C_C1_TX_MASK
#define I2C_Start()            I2C0_C1 |= I2C_C1_TX_MASK; I2C0_C1 |= I2C_C1_MST_MASK
#define I2C_stop()             I2C0_C1 &= ~I2C_C1_MST_MASK; I2C0_C1 &= ~I2C_C1_TX_MASK
#define I2C_Wait()             while((I2C0_S1 & I2C_S_IICIF_MASK)==0) {} I2C0_S1 |= I2C_S_IICIF_MASK;


void Pause(){
    int n;
    for(n=1;n<50;n++) {
        asm("nop");
    }
}

void I2C_begin(void){
	SIM_SCGC |= SIM_SCGC_I2C0_MASK; //Habilita I2C0
	SIM_PINSEL0 &= ~SIM_PINSEL_I2C0PS_MASK;
	I2C0_F  = 0x1B;
	I2C0_C1 = I2C_C1_IICEN_MASK;
    // I2C0_C1 |= I2C_C1_IICIE_MASK;
    PORT_PUE0 |= 0b1100 ;
}

void I2C_write(char value){
//Send <value> as a byte and wait for ACK bit
    I2C0_D = value;
    I2C_Wait();
}

void I2C_beginTransmission (char SlaveID){
//Send START bit, then slave address in write mode and then wait for ACK bit
    I2C0_S1 |= I2C_S_IICIF_MASK;
    I2C0_SMB &= ~I2C_SMB_FACK_MASK;
	I2C_Start();
	I2C_write((SlaveID << 1) & 0xFE );
}

void I2C_initRead(char SlaveID, char RA, int single){
//Initiate reading protocol to <SlaveID> at register address <RA>
//If only one byte is to be read, then <single> must be set
    I2C_beginTransmission(SlaveID);
    I2C_write(RA);
    I2C_RepeatedStart();
    I2C_write((SlaveID << 1) | 1);
    I2C_EnterRxMode();
    if(single) I2C_DisableAck();
    else I2C_EnableAck();
    char value = I2C0_D; //Dummy read
    I2C_Wait();
}

char I2C_read(int left){
//Return value read
//Indicate how many times <left> you intend to read from the same slave
    int value;
	switch(left){
    case 1:
        value = I2C0_D;
        I2C_Wait();
        I2C_DisableAck();
        break;
    case 0:
        I2C_stop();
        value = I2C0_D;
        break;
    default:
        value = I2C0_D;
        I2C_Wait();
	}
    Pause();
	return value;
}


int main(void){

    // Configurar Clock:
    clockSetup(CORE_24MHZ_BUS_12MHZ);

	const char MPU = 0x68;
	int AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;

	I2C_begin();

	I2C_beginTransmission(MPU);
	I2C_write(0x6B);
	I2C_write(0x0);
	I2C_stop();

	while(1){
        I2C_initRead(MPU, 0x3B, 0);
        AcX=I2C_read(13)<<8|I2C_read(12); //0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
        AcY=I2C_read(11)<<8|I2C_read(10); //0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
        AcZ=I2C_read(9)<<8|I2C_read(8); //0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
        Tmp=I2C_read(7)<<8|I2C_read(6); //0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
        GyX=I2C_read(5)<<8|I2C_read(4); //0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
        GyY=I2C_read(3)<<8|I2C_read(2); //0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
        GyZ=I2C_read(1)<<8|I2C_read(0); //0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

        //printf("Acel. X = %d | Y = %d | Z = %d", AcX, AcY, AcZ);
        //printf(" | Giro. X = %d | Y = %d | Z = %d", GyX, GyY, GyZ);
        //printf(" | Temp = %f\n", ((float)Tmp)/340.00+36.53);

        Pause();
	}
}
