# NVIC

## Descrição

O módulo NVIC(Nested Vector Interrupt Controller) é um módulo integrado ao núcleo Cortex-M0+ do microcontrolador(isso entre outras coisas, significa que quem projetou esse módulo foi a própria ARM, juntamente com o processador propriamente dito e todas suas extensões opcionais) responsável por administrar todos os pedidos de interrupção feitos por outros módulos. É este módulo que decide quando o processamento comum será interrompido para tratar interrupções, de acordo com como foi configurado.

O módulo permite habilitar quais módulos podem enviar pedidos de interrupção, e definir uma prioridade para cada um, definindo assim se uma interrupção pode ser novamente interrompida para tratar outra de prioridade maior.

Mais informações sobre os módulos integrados ao núcleo Cortex-M0+ podem ser encontradas na documentação online do ARM, [neste link](http://infocenter.arm.com/help/topic/com.arm.doc.dui0662b/CIHFADFF.html), enquanto que mais informações sobre o próprio NVIC podem ser encontradas em dos subtópicos do link acima, [neste link](http://infocenter.arm.com/help/topic/com.arm.doc.dui0662b/CIHIGCIF.html).

## Habilitando interrupções

Para habilitar a interrupção de um módulo específico, usa-se o registrador **NVIC_ISER**. Escrever 1 em um dos bits do registrador habilita uma interrupção específica, escrever 0 não tem efeito, e a leitura indica se a interrupção está habilitada(1) ou desabilitada(0).

Para saber qual bit corresponde a interrupção desejada, deve-se consultar a tabela na página 57 do [Reference Manual](http://www.nxp.com/assets/documents/data/en/reference-manuals/MKE06P80M48SF0RM.pdf)(3.3.2.3 Interrupt channel assignments) e encontrar a linha para sua interrupção, o bit para habilitar a interrupção no registrador **NVIC_ISER** é o número na coluna IRQ.

## Desabilitando interrupções

Para desabilitar uma interrupção específica, usa-se o registrador **NVIC_ICER**. Escrever 1 em um dos bits desse registrador desabilita uma interrupção. A leitura funciona da mesma forma que **NVIC_ISER**, e lê 1 se a interrupção estiver habilitada.

O bit para cada interrupção pode ser encontrado da mesma forma que na seção acima.

## Forçando interrupções

Escrever 1 em um bit do registrador **NVIC_ISPR** sinalizará pendência no tratamento da interrupção correspondente, que será tratada da mesma forma que se o hardware tivesse requisitado a interrupção normalmente. Escrever 0 não surte efeito.

A leitura de um bit do registrador indica se existe alguma interrupção com tratamento pendente(1) ou não(0).

## Impedir tratamento de uma interrupção

Escrever 1 em um bit do registrador **NVIC_ICPR** limpa a pendência de uma interrupção ainda não tratada, portanto essa solicitação de interrupção em particular não será tratada.Se outra pendência aparecer novamente, ela será tratada normalmente, a menos que a pendência seja limpa novamente, ou a interrupção esteja desabilitada.

A leitura se comporta igualmente ao do registrador **NVIC_ISPR**.

## Definindo prioridades
**Importante:** Interrupções com prioridade de valor mais baixo são mais importantes e serão atendidas com maior prioridade.

Existem 8 registradores **NVIC_IPRn** usados para definir a priodade de interrupções. Cada registrador controla a prioridade de 4 interrupções. Para descobrir em qual registrador sua interrupção desejada pode ser configurada, visita-se novamente a mesma tabela usada para descobrir o bit corresponde nos demias registradores, mas desta vez, usa-se a coluna *NVIC IPR register number*, o número nesta coluna, será o numero no nome do registrador que usamos para definir a interrupção.

O primeiro bit(menos significativo) relevante para definir a prioridade pode ser econtrado através da seguinte fórmula:
LSB = 8 * (IRQ mod 4) + 6
onde IRQ é o número usado para identificar o bit nos outros registradores, também disponível na tabela. o próximo bit é o bit mais significativo, ou seja:
MSB = LSB + 1 = 8 * (IRQ mod 4) + 7

## Exemplo

Para habilitar as interrupções do módulo PWT, e definir que sua prioridade como o maior número possível(menos importante), usamos: 
`
	// Habilitar interrupções PWT para o NVIC:
	NVIC_ISER |= 1 << 29;
	// Definir prioridade da interrupção PWT:
	NVIC_IPR7 |= 0b11 << 14;
`
pois o seu número IRQ é 29, e seu registrador é o 7.

## Notas

* O nome da função para tratar a interrupção pode ser consultado no arquivo *kinetis_sysinit.c* na pasta Auxiliary, sabendo a posição da função no vetor de interrupção do micro, disponível na tabela usada nas seções anteriores.
