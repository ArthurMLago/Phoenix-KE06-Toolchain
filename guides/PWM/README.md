# Módulo FTM 

## Descrição 

O módulo FTM é um timer de dois a oito canais que suporta captura de entrada, saída e geração de sinais PWM que podem controlar um motor elétrico.
A contagem inicia do valor definido no registrador **CNTIN** e termina no valor definido por **MOD**. A largura do pulso é definida pelo valor em **CnV**.
Para definir o valor de **MOD** a seguinte fórmula é utilizada: 
				**(MOD - CNTIN + 0x01)*Tftm = Tcontagem**, 
na qual,
	*Tcontagem* é o período de contagem;
	*Tftm* é o inverso da frequência do microcontrolador(24MHz) dividida pelo prescaler.

Para correta utilização desse módulo, recomenda-se além de ler este README, leia a seção 26.4 - Functional Description do módulo FTM na página 420 do [Reference Manual](http://www.nxp.com/assets/documents/data/en/reference-manuals/MKE06P80M48SF0RM.pdf).

## Configurações 

### FTMx_SC
Contém a flag do status do overflow e bits de controle para configurar a interrupção, o FTM, a fonte do clock e o fator de divisão do clock (prescaler).

* **TOF** : Bit 7. Setada pelo hardware quando o contador passa do valor do registrador MOD. Se algum overflow ocorrer entre operações de leitura e escrita, a operação de escrita não possui algum efeito e, assim, o TOF continuará indicando que um overflow ocorreu. 

* **TOIE** : Bit 6. Habilita interrupções causadas por overflow quando setada como 1.

* **CPWMS** : Bit 5. Seleciona o modo de contagem do contador. Caso seja 1, o contador está no modo de contagem up-down. Caso contrário, está no modo up.

* **CLKS** : Bits 4 e 3. Seleciona uma das três possíveis fontes de clock.
  	     00 - Não há clock selecionado (desabilita o contador);
	     01 - Clock do sistema;
	     10 - Clock de frequência fixada;
	     11 - Clock externo.

* **PS** : Bits 2 a 0. Seleciona um dos oito possíveis fatores de divisão para a fonte do clock selecionada em CLKS. O valor do prescaler, que pode variar de 000 a 111, é equivalente a potência de dois do valor atribuído ao registrador. É importante notar que quanto maior o prescaler, menor é a precisão da contagem.

### FTMx_CNT 
Contém o valor do contador do FTM.

### FTMx_MOD
Contém o valor do modulo para o contador FTM. Após o contador atingir o valor do modulo, a flag TOF é setada no próximo clock e o próximo valor do contador depende do método de contagem selecionado.

### FTMx_CnSC
Contém a flag de status da interrupção do canal e controla bits utilizados para configurar o enable da interrupção, a configuração do canal e a função do pino.

* **MSB** : Seleciona modo do canal.

* **ELSB** : Seleciona a borda do canal.

Para melhor compreender os valores dos pinos deve-se consultar a tabela 26.69 na página 383 do [Reference Manual](http://www.nxp.com/assets/documents/data/en/reference-manuals/MKE06P80M48SF0RM.pdf).

### FTMx_CnV 
Contém o valor do contador FTM capturado para os modos de entrada ou para os modos de saída.

### FTMx_CNTIN 
Contém o valor inicial do contador.


## Exemplos de uso

*Os exemplos de uso selecionados foram escolhidos julgando o que seria mais comumulmente utilizado dentro das atividades da equipe. Portanto, apenas os registradores relacionados ao PWM foram utilizados.*

### Utilização simples

Nesse exemplo, o PWM teve um duty cycle para variar de 0 a 100 (0 a 1) em 1.5 ms.

*Exemplo disponível no arquivo pwm.c*

