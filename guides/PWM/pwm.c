#include "derivative.h" /* include peripheral declarations */
#include "clkConfig.h"
/*
 *fclk = 24MHz
 *Tcontador = 2.0ms
 *MOD = 2^16 - 1
 *CNTIN = 0
 */
#define MOD 65535 // (MOD - CNTIN + 0x01)*(PS/fclk) = Tcontagem

#define ClockSpeed CORE_24MHZ_BUS_12MHZ

void delay(int time){
  int counter = 0;
  while(counter < time)
    counter += 1;
}

void init_FMT(){
  SIM_SCGC |= SIM_SCGC_FTM2_MASK ; //Ativa clk para FTM2

  FTM2_SC |= FTM_SC_PS(0); //Prescaler: fator que divide o clock por 1 
  FTM2_SC |= FTM_SC_CLKS(0x01); //Escolhe o clock do sistema para alimentar o FTM


  FTM2_CNTIN = 0; //Define valor de recarga para o contador
  FTM2_MOD = MOD; //Valor maximo do contador
  FTM2_C0SC |= FTM_CnSC_MSB_MASK;  //PWM Edge-Aligned
  FTM2_C0SC |= FTM_CnSC_ELSB_MASK; //Pulso nivel alto verdadeiro
  // FTM0_SC |=  FTM_SC_TOIE_MASK; //Timer Overflow enable 
  FTM2_C0V = FTM_CnV_VAL(500); //Seta a % da interrupcao - 50%

  SIM_PINSEL1 &= ~0x03; //Seleciona em qual pino a saída do canal deve ser mapeada, no caso PTC0
}


int main(){
  clockSetup(ClockSpeed);

  init_FMT();

  // Configurar pinos de PTA para piscar, só para poder acompanhar:
  GPIOA_PDDR |= 0xff;
  GPIOA_PIDR |= 0xff;
  GPIOA_PTOR |= 0xff;
  // asm("bkpt");


	
  float intensidade = 0.0;
  while(1){
    delay(500000); //delay de 0.5s
    while(intensidade <= 1.0){
      intensidade += 0.1;
      FTM2_C0V = FTM_CnV_VAL(intensidade*MOD / 1.0);
      delay(100);
    }
    // asm("bkpt");

    GPIOA_PTOR |= 0xff;
    intensidade = 0.0;
    // PWT_R1 |= 0x2;
    // asm("bkpt");
  }
}
